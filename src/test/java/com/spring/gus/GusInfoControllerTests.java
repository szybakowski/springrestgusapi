package com.spring.gus;

import com.spring.gus.Client.Exception.GusApiException;
import com.spring.gus.Model.GusInfo;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class GusInfoControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getGusInfoByNIP() throws Exception {
        GusInfo body = restTemplate.getForObject("/api/getInfoBy/nip/5261040828", GusInfo.class);
        assertThat(body.getRegon()).isEqualTo("000331501");
        assertThat(body.getName()).isEqualTo("GŁÓWNY URZĄD STATYSTYCZNY");
    }

    @Test
    public void getGusInfoByREGON() throws Exception {
        GusInfo body = restTemplate.getForObject("/api/getInfoBy/regon/000331501", GusInfo.class);
        assertThat(body.getNip()).isEqualTo("5261040828");
        assertThat(body.getName()).isEqualTo("GŁÓWNY URZĄD STATYSTYCZNY");
    }

    @Test
    public void getGusInfoByKRS() throws Exception {
        GusInfo body = restTemplate.getForObject("/api/getInfoBy/krs/0000240611", GusInfo.class);
        assertThat(body.getName()).isEqualTo("GOOGLE POLAND SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ");
        assertThat(body.getNip()).isEqualTo("5252344078");
        assertThat(body.getRegon()).isEqualTo("140182840");
    }

    @Test
    public void getGusInfoByKRS_NotFound() throws Exception {
        GusApiException body = restTemplate.getForObject("/api/getInfoBy/regon/00033150", GusApiException.class);
        assertThat(body.getMessage()).isEqualTo("No data found for the specified search criteria.");
    }

}

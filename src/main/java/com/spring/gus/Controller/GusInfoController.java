package com.spring.gus.Controller;

import com.spring.gus.Client.Exception.GusApiException;
import com.spring.gus.Model.GusInfo;
import com.spring.gus.Service.GusInfoService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ApiResponses(value = {
    @ApiResponse(code = 200, message = "Success", response = GusInfo.class),
        @ApiResponse(code = 404, message = "Company Not Found", response = GusApiException.class),
        @ApiResponse(code = 500, message = "Failure")
})
@RequestMapping("api/getInfoBy")
public class GusInfoController {

    @Autowired
    GusInfoService gusInfoService;

    @ApiOperation(value = "Get GusInfo by NIP", nickname = "Get GusInfo by NIP")
    @ApiImplicitParam(name = "searchingNIP", value = "Looking Company NIP", required = true, dataType = "String", paramType = "path", defaultValue = "5261040828")
    @GetMapping("/nip/{searchingNIP}")
    public GusInfo getGusInfoByNip(@PathVariable String searchingNIP) {
        return gusInfoService.getGusInfoByNIP(searchingNIP);
    }

    @ApiOperation(value = "Get GusInfo by REGON", nickname = "Get GusInfo by REGON")
    @ApiImplicitParam(name = "searchingREGON", value = "Looking Company REGON", required = true, dataType = "String", paramType = "path", defaultValue = "000331501")
    @GetMapping("/regon/{searchingREGON}")
    public GusInfo getGusInfoByREGON(@PathVariable String searchingREGON) {
        return gusInfoService.getGusInfoByREGON(searchingREGON);
    }

    @ApiOperation(value = "Get GusInfo by KRS", nickname = "Get GusInfo by KRS")
    @ApiImplicitParam(name = "searchingKRS", value = "Looking Company KRS", required = true, dataType = "String", paramType = "path", defaultValue = "0000240611")
    @GetMapping("/krs/{searchingKRS}")
    public GusInfo getGusInfoByKRS(@PathVariable String searchingKRS) {
        return gusInfoService.getGusInfoByKRS(searchingKRS);
    }
}

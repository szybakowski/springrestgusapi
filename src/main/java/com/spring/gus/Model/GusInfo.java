package com.spring.gus.Model;

public class GusInfo {

    private String regon;
    private String nip;
    private String name;
    private String voivodeship;
    private String district;
    private String community;
    private String city;
    private String zipCode;
    private String street;
    private String propertyNumber;
    private String apartmentNumber;
    private String errorMessage;

    @Override
    public String toString() {
        return "GusInfo{" + "regon=" + regon + ", nip=" + nip + ", name=" + name + ", voivodeship=" + voivodeship + ", district=" + district + ", community=" + community + ", city=" + city + ", zipCode=" + zipCode + ", street=" + street + ", propertyNumber=" + propertyNumber + ", apartmentNumber=" + apartmentNumber + ", errorMessage=" + errorMessage + '}';
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getRegon() {
        return regon;
    }

    public void setRegon(String regon) {
        this.regon = regon;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVoivodeship() {
        return voivodeship;
    }

    public void setVoivodeship(String voivodeship) {
        this.voivodeship = voivodeship;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPropertyNumber() {
        return propertyNumber;
    }

    public void setPropertyNumber(String propertyNumber) {
        this.propertyNumber = propertyNumber;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(String apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

}

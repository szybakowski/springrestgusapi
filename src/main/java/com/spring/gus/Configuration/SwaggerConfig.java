package com.spring.gus.Configuration;

import com.google.common.base.Predicate;
import java.util.Arrays;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import static springfox.documentation.builders.PathSelectors.regex;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket SwaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("springRestGus-api")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.spring.gus.Controller"))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Spring Rest Gus API")
                .description("Spring Rest Gus API is for getting Company information by basic identification data like:"
                        + "\n- NIP"
                        + "\n- REGON"
                        + "\n- KRS"
                        + "\n\nfrom Główny Urząd Statystyczny.")
                .contact(new Contact("Bartosz Sz",
                        "https://www.linkedin.com/in/bartosz-szybakowski-378033167/",
                        "szybakowski.b@gmail.com"))
                .license("")
                .licenseUrl("")
                .version("1.0")
                .build();
    }

}

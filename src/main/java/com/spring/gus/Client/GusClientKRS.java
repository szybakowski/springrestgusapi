package com.spring.gus.Client;

import com.spring.gus.Model.GusInfo;

public class GusClientKRS extends GusClient {

    public GusClientKRS(String searchingParam) {
        setParametryWyszukiwaniaEnum();
        setSearchingParam(searchingParam);
    }

    public GusInfo getGusInfoByParamKRS() {
        return getGusInfoByParam();
    }

    @Override
    void setParametryWyszukiwaniaEnum() {
        this.parametrWyszukiwaniaEnum = ParametryWyszukiwaniaEnum.KRS;
    }

    @Override
    void setSearchingParam(String searchingParam) {
        this.searchingParam = searchingParam;
    }

}

package com.spring.gus.Client;

import com.spring.gus.Model.GusInfo;

public class GusClientNIP extends GusClient {

    public GusClientNIP(String searchingParam) {
        setParametryWyszukiwaniaEnum();
        setSearchingParam(searchingParam);
    }

    public GusInfo getGusInfoByParamNIP() {
        return getGusInfoByParam();
    }

    @Override
    void setParametryWyszukiwaniaEnum() {
        this.parametrWyszukiwaniaEnum = ParametryWyszukiwaniaEnum.NIP;
    }

    @Override
    void setSearchingParam(String searchingParam) {
        this.searchingParam = searchingParam;
    }
}

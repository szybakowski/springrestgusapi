package com.spring.gus.Client;

import com.spring.gus.Model.GusInfo;

public class GusClientREGON extends GusClient {

    public GusClientREGON(String searchingParam) {
        setParametryWyszukiwaniaEnum();
        setSearchingParam(searchingParam);
    }
    
        public GusInfo getGusInfoByParamREGON() {
        return getGusInfoByParam();
    }

    @Override
    void setParametryWyszukiwaniaEnum() {
        this.parametrWyszukiwaniaEnum = ParametryWyszukiwaniaEnum.REGON;
    }

    @Override
    void setSearchingParam(String searchingParam) {
        this.searchingParam = searchingParam;
    }

}

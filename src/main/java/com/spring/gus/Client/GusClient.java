package com.spring.gus.Client;

import com.spring.gus.Model.GusInfo;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import javax.xml.ws.soap.AddressingFeature;

import com.spring.gus.Schemas.IUslugaBIRzewnPubl;
import com.spring.gus.Schemas.ObjectFactory;
import com.spring.gus.Schemas.ParametryWyszukiwania;
import com.spring.gus.Schemas.UslugaBIRzewnPubl;
import org.springframework.stereotype.Component;

public abstract class GusClient {

    private String gusUserKey = "abcde12345abcde12345";
    public static String gusSessionIdentifier = "";
    public static UslugaBIRzewnPubl service = new UslugaBIRzewnPubl();

    protected ParametryWyszukiwaniaEnum parametrWyszukiwaniaEnum;
    protected String searchingParam;

    protected GusInfo getGusInfoByParam() {
        IUslugaBIRzewnPubl port = service.getE3(new AddressingFeature());
        String lastGusSessionIdentifier = gusSessionIdentifier;
        String response = "";

        if (gusSessionIdentifier.equals("") || !port.getValue("StatusUslugi").equals("1")) {
            gusSessionIdentifier = port.zaloguj(gusUserKey);

        } else if (port.getValue("StatusSesji").equals("0")) {
            gusSessionIdentifier = port.zaloguj(gusUserKey);
        }

        System.out.println("lastGusSessionIdentifier: " + lastGusSessionIdentifier);
        System.out.println("gusSessionIdentifier: " + gusSessionIdentifier);

        if (gusSessionIdentifier.equals(lastGusSessionIdentifier)) {
            response = port.daneSzukajPodmioty(getParametryWyszukiwaniaEnum());

        } else {
            service.setHandlerResolver(new HandlerResolver() {
                @Override
                public List<Handler> getHandlerChain(PortInfo portInfo) {
                    final List<Handler> handlerList = new ArrayList<Handler>();
                    handlerList.add(new SoapHandlerResolver(gusSessionIdentifier));
                    return handlerList;
                }
            });

            IUslugaBIRzewnPubl port2 = service.getE3(new AddressingFeature());
            response = port2.daneSzukajPodmioty(getParametryWyszukiwaniaEnum());
        }

        return castResponseToGusInfo(response);
    }

    private ParametryWyszukiwania getParametryWyszukiwaniaEnum() {
        ParametryWyszukiwania result = new ParametryWyszukiwania();
        ObjectFactory objectFactory = new ObjectFactory();

        switch (parametrWyszukiwaniaEnum.getValue()) {
            case 1:
                JAXBElement<String> nipParam = objectFactory.createParametryWyszukiwaniaNip(searchingParam);
                result.setRegon(nipParam);
                break;
            case 2:
                JAXBElement<String> regonParam = objectFactory.createParametryWyszukiwaniaRegon(searchingParam);
                result.setRegon(regonParam);
                break;
            case 3:
                JAXBElement<String> krsParam = objectFactory.createParametryWyszukiwaniaKrs(searchingParam);
                result.setRegon(krsParam);
                break;
        }

        return result;
    }

    private GusInfo castResponseToGusInfo(String response) {
        GusInfo result = new GusInfo();

        result.setRegon(getValueFromString("<Regon>", "</Regon>", response));
        result.setNip(getValueFromString("<Nip>", "</Nip>", response));
        result.setName(getValueFromString("<Nazwa>", "</Nazwa>", response));
        result.setVoivodeship(getValueFromString("<Wojewodztwo>", "</Wojewodztwo>", response));
        result.setDistrict(getValueFromString("<Powiat>", "</Powiat>", response));
        result.setCommunity(getValueFromString("<Gmina>", "</Gmina>", response));
        result.setCity(getValueFromString("<Miejscowosc>", "</Miejscowosc>", response));
        result.setZipCode(getValueFromString("<KodPocztowy>", "</KodPocztowy>", response));
        result.setStreet(getValueFromString("<Ulica>", "</Ulica>", response));
        result.setPropertyNumber(getValueFromString("<NrNieruchomosci>", "</NrNieruchomosci>", response));
        result.setApartmentNumber(getValueFromString("<NrLokalu>", "</NrLokalu>", response));
        result.setErrorMessage(getValueFromString("<ErrorMessageEn>", "</ErrorMessageEn>", response));

        System.out.println(response);
        System.out.println(result.toString());
        return result;
    }

    private String getValueFromString(String beginStrig, String endString, String fullString) {
        String result = "";

        try {
            result = fullString.substring(fullString.indexOf(beginStrig) + beginStrig.length(), fullString.indexOf(endString));
        } catch (Exception e) {
        }

        return result;
    }

    abstract void setParametryWyszukiwaniaEnum();

    abstract void setSearchingParam(String searchingParam);
}

package com.spring.gus.Client.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class GusApiException extends RuntimeException {

    public GusApiException(String errorMessage) {
        super(errorMessage);
    }

}

package com.spring.gus.Client;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.List;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class SoapHandlerResolver implements SOAPHandler<SOAPMessageContext> {

    private String sessionIdentifier = "";

    public SoapHandlerResolver(String sid) {
        this.sessionIdentifier = sid;
    }

    @Override
    public void close(MessageContext arg0) {
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public Set getHeaders() {
        return null;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return false;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext soapMessageContext) {
        Map<String, List<String>> headers = (Map<String, List<String>>) soapMessageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
        headers = new HashMap<String, List<String>>();
        headers.put("sid", Collections.singletonList(sessionIdentifier));
        soapMessageContext.put(MessageContext.HTTP_REQUEST_HEADERS, headers);

        return true;
    }

}

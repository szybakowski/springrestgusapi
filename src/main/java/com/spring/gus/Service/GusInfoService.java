package com.spring.gus.Service;

import com.spring.gus.Client.GusClientKRS;
import com.spring.gus.Client.GusClientREGON;
import com.spring.gus.Client.GusClientNIP;
import com.spring.gus.Client.Exception.GusApiException;
import com.spring.gus.Model.GusInfo;
import org.springframework.stereotype.Service;

@Service
public class GusInfoService {

    public GusInfo getGusInfoByNIP(String searchingNIP) {
        return checkGusInfoResponse(new GusClientNIP(searchingNIP).getGusInfoByParamNIP());
    }

    public GusInfo getGusInfoByREGON(String searchingREGON) {
        return checkGusInfoResponse(new GusClientREGON(searchingREGON).getGusInfoByParamREGON());
    }

    public GusInfo getGusInfoByKRS(String searchingKRS) {
        return checkGusInfoResponse(new GusClientKRS(searchingKRS).getGusInfoByParamKRS());
    }

    private GusInfo checkGusInfoResponse(GusInfo gusInfo) {
        if (!gusInfo.getErrorMessage().equals("")) {
            throw new GusApiException(gusInfo.getErrorMessage());
        }

        return gusInfo;
    }
}

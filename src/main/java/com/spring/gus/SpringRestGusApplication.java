package com.spring.gus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestGusApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringRestGusApplication.class, args);
    }
}

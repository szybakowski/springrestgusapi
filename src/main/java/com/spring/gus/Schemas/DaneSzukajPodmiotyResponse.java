
package com.spring.gus.Schemas;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DaneSzukajPodmiotyResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "daneSzukajPodmiotyResult"
})
@XmlRootElement(name = "DaneSzukajPodmiotyResponse")
public class DaneSzukajPodmiotyResponse {

    @XmlElementRef(name = "DaneSzukajPodmiotyResult", namespace = "http://CIS/BIR/PUBL/2014/07", type = JAXBElement.class, required = false)
    protected JAXBElement<String> daneSzukajPodmiotyResult;

    /**
     * Gets the value of the daneSzukajPodmiotyResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDaneSzukajPodmiotyResult() {
        return daneSzukajPodmiotyResult;
    }

    /**
     * Sets the value of the daneSzukajPodmiotyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDaneSzukajPodmiotyResult(JAXBElement<String> value) {
        this.daneSzukajPodmiotyResult = value;
    }

}
